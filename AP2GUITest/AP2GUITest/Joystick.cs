﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AP2GUITest
{
    class Joystick : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double initX;
        private double initY;
        private double x;
        private double y;
        private bool isPressed;
        public double X
        {
            get
            {
                return x;
            }
            set
            {
                this.x = value;
                NotifyPropertyChanged("X");
            }
        }
        public double Y
        {
            get
            {
                return y;
            }
            set
            {
                this.y = value;
                NotifyPropertyChanged("Y");
            }
        }
        public bool IsPressed
        {
            get
            {
                return isPressed;
            }
            set
            {
                isPressed = value;
                NotifyPropertyChanged("IsPressed");
            }
        }

        public Joystick(double initialX, double initalY)
        {
            initX = initialX;
            initY = initalY;
            RestartJoystickLocation();
        }

        public void RestartJoystickLocation()
        {
            X = initX;
            Y = initY;
        }
        
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
