﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AP2GUITest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Joystick js;
        double joystickRadius;
        double joystickPadRadius;
        double joystickInitLocation;
        public MainWindow()
        {
            InitializeComponent();
            joystickRadius = Joystick.Height / 2;
            joystickPadRadius = JoystickPad.Height / 2;
            joystickInitLocation = Canvas.GetTop(JoystickPad) + joystickPadRadius;
            js = new Joystick(joystickInitLocation, joystickInitLocation); 
            InitJoystickLocation();
        }

        private void Joystick_MouseMove(object sender, MouseEventArgs e)
        {
            if (js.IsPressed)
            {
                UpdateJoystickLocation(e);
            }
        }

        private void UpdateJoystickLocation(MouseEventArgs e)
        {
            if (e.GetPosition(JoystickCanvas).X > Canvas.GetLeft(JoystickPad) 
                && e.GetPosition(JoystickCanvas).Y > Canvas.GetTop(JoystickPad)
                && e.GetPosition(JoystickCanvas).X < Canvas.GetLeft(JoystickPad) + joystickPadRadius * 2
                && e.GetPosition(JoystickCanvas).Y < Canvas.GetTop(JoystickPad) + joystickPadRadius * 2)
            {
                js.Y = e.GetPosition(JoystickCanvas).Y;
                js.X = e.GetPosition(JoystickCanvas).X;
                Canvas.SetTop(Joystick, js.Y - joystickRadius);
                Canvas.SetLeft(Joystick, js.X - joystickRadius);
                UpdateTextBoxes();
            }
        }

        private void UpdateTextBoxes()
        {
            JoystickX.Text = ((js.X - joystickInitLocation) / joystickInitLocation).ToString();
            JoystickY.Text = ((js.Y - joystickInitLocation) / joystickInitLocation).ToString();
        }

        private void InitJoystickLocation()
        {
            js.RestartJoystickLocation();
            Canvas.SetTop(Joystick, js.Y - joystickRadius);
            Canvas.SetLeft(Joystick, js.X - joystickRadius);
            UpdateTextBoxes();
        }

        private void Joystick_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            js.IsPressed = false;
            InitJoystickLocation();
        }

        private void Joystick_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            js.IsPressed = true;
            UpdateJoystickLocation(e);
        }
    }
}
