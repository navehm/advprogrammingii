﻿using FlightSimulator.Model;
using FlightSimulator.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.ViewModels
{
    /// <summary>
    /// The abstraction unit for connecting the AutoPilot view with the model.
    /// the commandHandlers for the buttons are set in here.
    /// the isEdited property used for changing the TextBox background color.
    /// </summary>
    class AutoPilotViewModel : BaseNotify
    {
        private IAutoPilotModel model;
        public CommandHandler SendCommand { get; private set; }
        public CommandHandler ClearCommand { get; private set; }
        private Boolean isedited;
        public Boolean isEdited
        {
            get { return isedited; }
            private set
            {
                isedited = value;
                NotifyPropertyChanged("isEdited");
            }
        }
        public String VM_AutoPilotText
        {
            get { return model.AutoPilotText;  }
            set
            {
                isEdited = true;
                model.AutoPilotText = value;
                NotifyPropertyChanged("VM_AutoPilotText");
            }
        }

        
        public AutoPilotViewModel(IAutoPilotModel autoPilotModel)
        {
            model = autoPilotModel;
            isEdited = false;
            SendCommand = new CommandHandler(() =>
            {
                model.SendToServer();
                isEdited = false;
            });
            ClearCommand = new CommandHandler(() =>
            {
                VM_AutoPilotText = "";
                NotifyPropertyChanged("VM_AutoPilotText");
                isEdited = false;
            });
        }
        
    }
}
