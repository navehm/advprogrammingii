﻿using FlightSimulator.Model;
using FlightSimulator.Model.Interface;
using FlightSimulator.Model.Network;
using FlightSimulator.ViewModels.Windows;
using FlightSimulator.Views.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlightSimulator.ViewModels
{
    /// <summary>
    ///  The Main View Model. Contains all the other ViewModels as properties, and all the models as private variables.
    ///  Set the CommandHandlers for the Setting, Connect and Disconnect buttons.
    /// </summary>
    class MainWindowViewModel : BaseNotify
    {
        public AutoPilotViewModel VM_AutoPilot { get; private set; }
        public ManualControlViewModel VM_ManualControl { get; private set; }
        public FlightBoardViewModel VM_FlightBoard { get; private set; }
        public SettingsWindowViewModel VM_SettingsWindow { get; private set; }
        private IAutoPilotModel autoPilotModel;
        private IManualControlModel manualControlModel;
        private IFlightBoardModel flightBoardModel;
        private ISettingsModel settingsModel;

        private Boolean isConnected;

        public CommandHandler ConnectCommand { get; private set; }
        public CommandHandler DisconnectCommand { get; private set; }
        public CommandHandler SettingCommand { get; private set; }

        private IInfoServer infoServer;
        private ICommandServer commandServer;

        public MainWindowViewModel()
        {
            isConnected = false;
            settingsModel = new ApplicationSettingsModel();
            VM_SettingsWindow = new SettingsWindowViewModel(settingsModel);
            VM_SettingsWindow.ReloadSettings();
            autoPilotModel = new AutoPilotModel();
            VM_AutoPilot = new AutoPilotViewModel(autoPilotModel);
            manualControlModel = new ManualControlModel();
            VM_ManualControl = new ManualControlViewModel(manualControlModel);
            VM_FlightBoard = new FlightBoardViewModel();
            flightBoardModel = new FlightBoardModel(VM_FlightBoard);
            ConnectCommand = new CommandHandler(() =>
            {
                if (!isConnected)
                {
                    infoServer = new InfoServer(VM_SettingsWindow.FlightServerIP, VM_SettingsWindow.FlightInfoPort);
                    commandServer = new CommandServer(VM_SettingsWindow.FlightServerIP, VM_SettingsWindow.FlightCommandPort);
                    commandServer.Connect();
                    autoPilotModel.Server = commandServer;
                    manualControlModel.Server = commandServer;
                    flightBoardModel.Server = infoServer;
                    flightBoardModel.StartRecievingInfo();
                    autoPilotModel.Start();
                    isConnected = true;
                }
            });
            DisconnectCommand = new CommandHandler(() =>
            {
                if (isConnected)
                {
                    autoPilotModel.Stop();
                    commandServer.Disconnect();
                    flightBoardModel.StopRevievingInfo();
                    autoPilotModel.Server = null;
                    manualControlModel.Server = null;
                    flightBoardModel.Server = null;
                    isConnected = false;
                }
            });
            SettingCommand = new CommandHandler(() =>
            {
                SettingsWindow settings = new SettingsWindow();
                settings.DataContext = VM_SettingsWindow;
                settings.ShowDialog();
            });
        }
    }
}
