﻿using FlightSimulator.Model;
using FlightSimulator.Model.EventArgs;
using FlightSimulator.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.ViewModels
{
    /// <summary>
    /// The abstraction unit connecting between the manual control view and the model.
    /// When returning the Aileron and Elevator property, edit them inorder to keep them between 0-360 and 0-100 accordingly
    /// instead of between -1-1 as used by the FlightGear.
    /// </summary>
    class ManualControlViewModel : BaseNotify
    {
        IManualControlModel model;
        
        public ManualControlViewModel(IManualControlModel manualControlModel)
        {
            model = manualControlModel;
        }

        public double VM_Aileron
        {
            get { return (model.Aileron + 1) * 180; }
            set
            {
                model.Aileron = value;
                NotifyPropertyChanged("VM_Aileron");
            }
        }

        public double VM_Elevator
        {
            get { return (model.Elevator + 1) * 50; }
            set
            {
                model.Elevator = value;
                NotifyPropertyChanged("VM_Elevator");
            }
        }

        public double VM_Throttle
        {
            get { return model.Throttle; }
            set
            {
                model.Throttle = value;
                NotifyPropertyChanged("VM_Throttle");
            }
        }

        public double VM_Rudder
        {
            get { return model.Rudder; }
            set
            {
                model.Rudder = value;
                NotifyPropertyChanged("VM_Rudder");
            }
        }

        public void VM_JoystickMoved(object sender, VirtualJoystickEventArgs e)
        {
            VM_Aileron = e.Aileron;
            VM_Elevator = e.Elevator;
        }

        public void VM_JoystickReleased(object sender)
        {
            VM_Aileron = 180;
            VM_Elevator = 50;
        }
    }
}
