﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Geometry
{
    class Grid
    {
        private int radios;
        private bool[,] table;

        public Grid(int r)
        {
            radios = r;
            table = new Boolean[2*r,2*r];
            table.Initialize();
        }

        public override string ToString()
        {
            string ret = "";
            for (int i = 0; i<2*radios; i++)
            {
                for (int j=0; j<2*radios; j++)
                {
                    ret += table[i, j] ? "O" : " ";
                }
                ret += "\r\n";
            }
            return ret;
        }

        public void SetPoint(Point p)
        {
            try
            {
                table[radios - p.Y, radios + p.X] = table[radios - p.Y, radios + p.X] ? false : true ;
            } catch (Exception e) { }
        }
    }
}
