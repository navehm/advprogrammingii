﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Geometry;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point();
            ConsoleKeyInfo k;
            Grid grid = new Grid(10);
            do
            {
                grid.SetPoint(p);
                Console.Clear();
                Console.WriteLine(grid);
                k = Console.ReadKey();
                switch (k.Key)
                {
                    case ConsoleKey.UpArrow:
                        p = p + new Point(0, 1);
                        break;
                    case ConsoleKey.DownArrow:
                        p = p - new Point(0, 1);
                        break;
                    case ConsoleKey.LeftArrow:
                        p = p - new Point(1, 0);
                        break;
                    case ConsoleKey.RightArrow:
                        p = p + new Point(1, 0);
                        break;
                }
            } while (k.Key != ConsoleKey.Q);
        }
    }
}
